﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Speech.Synthesis;
using System.Windows.Forms;
using System.Linq;
using static JNSoundboard.AddEditHotkeyForm;

namespace JNSoundboard
{
    public partial class TextToSpeechForm : Form
    {
        MainForm mainForm;
        SpeechSynthesizer synth = new SpeechSynthesizer();
        Prompt previewPrompt;

        public VoiceGender Gender { get; set; } = VoiceGender.Neutral;

        public TextToSpeechForm()
        {
            InitializeComponent();
        }

        private void TTS_Load(object sender, EventArgs e)
        {
            mainForm = Application.OpenForms[0] as MainForm;

            PopulateDropdowns();
        }

        private void PopulateDropdowns()
        {
            //Used to contain an Age dropdown, too. Didn't do anything. Maybe if someone has some sort of special TTS provider it works. Might add it back later.
            //As for gender, the other two options are NotSet (male) and Neutral (female), so they're redundant. Again, above might apply.

            cbGender.Items.Add( "Male" );
            cbGender.Items.Add( "Female" );

            cbGender.SelectedIndex = 0;
        }

        private void btnBrowseFolderLoc_Click(object sender, EventArgs e)
        {
            var diag = new SaveFileDialog();
            diag.Filter = "WAV files|*.wav";

            if (diag.ShowDialog() == DialogResult.OK)
            {
                tbWhereSave.Text = diag.FileName;
            }
        }

        private void btnCreateWAV_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbText.Text) && !string.IsNullOrWhiteSpace(tbWhereSave.Text) && Directory.Exists( Path.GetDirectoryName( Path.GetDirectoryName ( tbWhereSave.Text ) ) ) )
            {
                string path = tbWhereSave.Text;

                synth.SelectVoiceByHints( Gender, VoiceAge.NotSet );
                synth.SetOutputToWaveFile(path);

                PromptBuilder builder = new PromptBuilder();
                builder.AppendText(tbText.Text);

                synth.Speak(builder);

                MessageBox.Show("Saved: " + path);
            }
            else
            {
                MessageBox.Show("No text in text box and/or where to save box... or the where to save folder does not exist");
            }
        }

        private void btnCreateWAVAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbText.Text) && !string.IsNullOrWhiteSpace(tbKeys.Text) && !string.IsNullOrWhiteSpace(tbWhereSave.Text) && Directory.Exists(tbWhereSave.Text))
            {
                Keys[] convertedKeys;
                string error;

                if (Helper.keysArrayFromString(tbKeys.Text, out convertedKeys, out error))
                {
                    if (convertedKeys.Length > 0)
                    {
                        var newSH = new XMLSettings.SoundHotkey(convertedKeys, "", new string[] { tbWhereSave.Text + "\\" + Helper.cleanFileName(tbText.Text.Replace(" ", "") + ".wav") });

                        synth.SelectVoiceByHints( Gender, VoiceAge.NotSet );
                        synth.SetOutputToWaveFile(newSH.SoundLocations[0]);

                        PromptBuilder builder = new PromptBuilder();
                        builder.AppendText(tbText.Text);

                        synth.Speak(builder);

                        synth.Dispose();
                        synth = null;

                        mainForm.soundHotkeys.Add(newSH);

                        var newItem = new ListViewItem(tbKeys.Text);
                        newItem.SubItems.Add(""); //window title
                        newItem.SubItems.Add(newSH.SoundLocations[0]);

                        mainForm.lvKeySounds.Items.Add(newItem);

                        mainForm.lvKeySounds.ListViewItemSorter = new ListViewItemComparer(0);
                        mainForm.lvKeySounds.Sort();

                        mainForm.soundHotkeys.Sort(delegate (XMLSettings.SoundHotkey x, XMLSettings.SoundHotkey y)
                        {
                            if (x.Keys == null && y.Keys == null) return 0;
                            else if (x.Keys == null) return -1;
                            else if (y.Keys == null) return 1;
                            else return Helper.keysToString(x.Keys).CompareTo(Helper.keysToString(y.Keys));
                        });

                        MessageBox.Show("Saved: " + newSH.SoundLocations[0]);
                    }
                }
                else
                {
                    MessageBox.Show("Keys string incorrectly made. Check for spelling errors");
                }
            }
            else
            {
                MessageBox.Show("No text in text box, keys box, and/or where to save box... or the where to save folder does not exist");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            StopPreview();
            this.Close();
        }

        private void tbKeys_Enter(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void tbKeys_Leave(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        int lastAmountPressed = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            int amountPressed = 0;

            if (Keyboard.IsKeyDown(Keys.Escape))
            {
                lastAmountPressed = 50;

                tbKeys.Text = "";
            }
            else
            {
                var pressedKeys = new List<Keys>();

                foreach (Keys key in Enum.GetValues(typeof(Keys)))
                {
                    if (Keyboard.IsKeyDown(key))
                    {
                        amountPressed++;
                        pressedKeys.Add(key);
                    }
                }

                if (amountPressed > lastAmountPressed)
                {
                    tbKeys.Text = Helper.keysToString(pressedKeys.ToArray());
                }

                lastAmountPressed = amountPressed;
            }
        }

        private void clearHotkey_Click( object sender, EventArgs e )
        {
            tbKeys.Text = "";
        }

        private void preview_Click( object sender, EventArgs e )
        {
            StopPreview();

            if ( !string.IsNullOrWhiteSpace( tbText.Text ) )
            {                
                synth.SelectVoiceByHints( Gender, VoiceAge.NotSet );
                previewPrompt = synth.SpeakAsync( tbText.Text );
            }
        }

        private void cbGender_SelectedIndexChanged( object sender, EventArgs e )
        {
            VoiceGender gender;

            if ( Enum.TryParse( (string) cbGender.SelectedItem, out gender ) )
            {
                Gender = gender;
            }
        }

        private void stopPreview_Click( object sender, EventArgs e )
        {
            StopPreview();
        }

        private void StopPreview()
        {
            synth.SpeakAsyncCancel( previewPrompt );
        }
    }
}
